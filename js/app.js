const React = require('react');
import { createHistory, createHashHistory, useBasename } from 'history'

import { Router, Route, IndexRoute, RoutingContext } from 'react-router';

const Shop = require('./components/Shop.react');
const Home = require('./components/Home.react');
const Products = require('./components/Products.react');
const ProductItem = require('./components/ProductItem.react');
const ProductPage = require('./components/ProductPage.react');
const CartPage = require('./components/CartPage.react');

const history = useBasename(createHistory)({
    basename: '/shop'
});


React.render(
    <Router history={history}>
        <Route path="/" component={Shop}>
            <IndexRoute component={Home}/>
            <Route path="products/:categoryId" component={Products} />
            <Route path="product/:id" component={ProductPage} />
            <Route path="cart" component={CartPage} />
        </Route>
    </Router>,
    document.getElementById('shop')
);


