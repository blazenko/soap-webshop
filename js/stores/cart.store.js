const _ = require('lodash');
const EventEmitter = require('events').EventEmitter;
const React = require('react/addons');
const AppDispatcher = require('../dispatcher/AppDispatcher');

const storage = require('../util/simple.storage');
const ShopStore = require('./shop.store.js');

const CHANGE_EVENT = 'change';
let items = [];
let itemId = 1;

const CartStore = _.assign({}, EventEmitter.prototype, {

    getCart() {
        console.log(items);
        return {
            id: null,
            items: items
        };
    },

    _setCartFromStorage(savedCart) {
        console.log("CartStore _setCartFromStorage - savedCart", savedCart);
        if (savedCart.items) {
            _.forEach(savedCart.items, item => {
                if (item.itemId >= itemId) {
                    itemId = item.itemId + 1;
                }
            });
            items = savedCart.items;
            console.log("---itemId after _setCartFromStorage", itemId);
        }
    },

    _addProduct(productId) {
        let existingItem = _.find(items, item => _.get(item, 'product.product_id') === productId);
        if (_.isUndefined(existingItem)) {
            items.push({
                itemId: itemId++,
                product: ShopStore.getProduct(productId),
                quantity: 1
            });
        }
        else {
            existingItem.quantity += 1;
        }
        console.log('CartStore._addProduct, cart:', this.getCart());
    },

    _removeProduct(itemId) {
        let existingItem = _.find(items, ci => ci.itemId === itemId);
        if (!_.isUndefined(existingItem)) {
            if (existingItem.quantity > 1) {
                existingItem.quantity -= 1;
            }
            else {
                items = _.filter(items, ci => ci.itemId !== itemId);
            }
        }
        console.log('CartStore._removeProduct, cart:', this.getCart());
    },

    _emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

CartStore.dispatchToken = AppDispatcher.register(function (payload) {
    switch (payload.actionType) {
        case 'setCartFromStorage':
            CartStore._setCartFromStorage(payload.cart);
            CartStore._emitChange();
            break;
        case 'selectProduct':
            CartStore._addProduct(payload.id);
            CartStore._emitChange();
            break;
        case 'unSelectProduct':
            CartStore._removeProduct(payload.id);
            CartStore._emitChange();
            break;
    }
});

module.exports = CartStore;



