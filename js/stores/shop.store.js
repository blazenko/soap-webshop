const _ = require('lodash');
const EventEmitter = require('events').EventEmitter;
const React = require('react/addons');

const AppDispatcher = require('../dispatcher/AppDispatcher');

const CHANGE_EVENT = 'change';
let _allProducts = [];
let _category = {};
let _categories = [];

const ShopStore = _.assign({}, EventEmitter.prototype, {

    getCategory() {
        return _category;
    },

    getCategoryList() {
        return _categories;
    },

    getProducts() {
        return _allProducts;
    },

    getProduct(id) {
        console.log('getProduct', id);
        return _.find(_allProducts, p => p.product_id == id);
    },

    _setProducts(prods) {
        _allProducts = prods;
    },

    _setCategory(cat) {
        _category = cat;
    },

    _setCategories(cats) {
        _categories = cats;
    },

    _emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

ShopStore.dispatchToken = AppDispatcher.register(function (payload) {
    switch (payload.actionType) {
        case 'gotAllProducts':
            ShopStore._setProducts(payload.products);
            ShopStore._emitChange();
            break;
        case 'gotCategory':
            ShopStore._setCategory(payload.category);
            ShopStore._emitChange();
            break;
        case 'gotCategories':
            ShopStore._setCategories(payload.categories);
            ShopStore._emitChange();
            break;
    }
});

module.exports = ShopStore;


