const _ = require('lodash');
const request = require('superagent');

const AppDispatcher = require('../dispatcher/AppDispatcher');
const storage = require('../util/simple.storage');

const ShopActions = {

    getProducts(catId) {
        if (catId) {
            console.log('getting CATEG products');
            request.get('/data/?get=Category&id='+catId)
                .end(function(err, res){
                    const data = JSON.parse(res.text);
                    AppDispatcher.dispatch({
                        actionType: 'gotCategory',
                        category: data
                    });
                });
        }
        else {
            console.log('getting ALL products');
            request.get('/data/?get=AllProducts')
                .end(function(err, res){
                    const data = JSON.parse(res.text);
                    AppDispatcher.dispatch({
                        actionType: 'gotAllProducts',
                        products: data
                    });
                });
        }
    },

    getCategoryList() {
        request.get('/data/?get=Categories')
            .end(function(err, res){
                const data = JSON.parse(res.text);
                AppDispatcher.dispatch({
                    actionType: 'gotCategories',
                    categories: data
                });
            });
    },

    readCartFromStorage() {
        if (storage('cart')) {
            AppDispatcher.dispatch({
                actionType: 'setCartFromStorage',
                cart: storage('cart')
            });
        }
    },

    saveCartToStorage(cart) {
        storage('cart', cart);
    },


    selectProduct(id) {
        AppDispatcher.dispatch({
            actionType: 'selectProduct',
            id: id
        });
    },

    unSelectProduct(id) {
        AppDispatcher.dispatch({
            actionType: 'unSelectProduct',
            id: id
        });
    }
};

module.exports = ShopActions;