/**
 * Created by demo on 21.11.2015..
 */
const React = require('react');

const Products = require('./Products.react');

const Home = React.createClass({
    render: function() {
        return (
            <div className="shop-home">
                <h4>Home</h4>
                <div>Additional content 1</div>
                <Products categoryId={1}/>
                <div>Additional content 2</div>
            </div>

        );
    }
});

module.exports = Home;
