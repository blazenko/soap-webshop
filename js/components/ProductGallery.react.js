const React = require('react/addons');
const _ = require('lodash');

const ProductGallery = React.createClass({
    propTypes: {
        images: React.PropTypes.array.isRequired
    },

    getDefaultProps() {
        return {
            images: []
        };
    },


    getInitialState() {
        return {
            imgIndex: 0
        };
    },

    componentDidMount() {
        this._showImage(0);
    },

    componentWillReceiveProps(nextProps) {
        this._showImage(0);
    },



    render() {
        if (_.isEmpty(this.props.images)) return null;

        const src = `/images/products/${this.props.images[this.state.imgIndex].filename}`;
        return (
            <div className="product-gallery">
                <div className="product-image-container">
                    <img id="product-main-image" src={src}/>
                </div>
                { this._renderThumbs() }
            </div>
        );
    },

    _renderThumbs() {
        let thumbs = _.map(this.props.images, (img, i) => {
            let src = `/images/products/${img.filename}`;
            return (
                <img
                     key={i}
                     className="product-thumb"
                     src={src}
                     onClick={e => this._showImage(i)}
                     onMouseEnter={this._thumbRollOver}
                     onMouseLeave={this._thumbRollOut}
                />
            );
        });
        if (this.props.images.length === 5) {
            const blank = <img src="/grfx/blank.png" className="product-thumb blank"/>;
            thumbs.unshift(blank);
            thumbs.unshift(blank);
        }
        return thumbs;
    },

    _showImage(idx) {
        this.setState({
            imgIndex: idx
        });
    },

    _thumbRollOver(e) {
        //console.log('roll over', e.target);
    },

    _thumbRollOut(e) {
        //console.log('roll out', e.target);
    },


});

module.exports = ProductGallery;

