/**
 * Created by demo on 19.11.2015..
 */

const React = require('react');
const _ = require('lodash');

const CartItem = require('./CartItem.react');

const Cart = React.createClass({

    propTypes: {
        cart: React.PropTypes.object
    },

    getDefaultProps() {
        return {
            cart: {
                id: null,
                items:[]
            }
        };
    },

    componentDidMount() {
        console.log("CartSidebar componentDidMount");
    },


    componentDidUpdate(prevProps, prevState) {
        console.log("CartSidebar componentDidUpdate");
        this._calcTotal();
    },



    render: function() {
        return (
            <div className="cart-sidebar">
                Vaša košarica
                <ul className="cart-sidebar-list">
                    {this._renderCartItems()}
                </ul>
                Total: {this._calcTotal()}
            </div>
        );
    },

    _renderCartItems() {
        if (_.isEmpty(this.props.cart.items)) {
            return 'Nema proizvoda u vašoj košarici.';
        }
        return _.map(this.props.cart.items, (item, i) => {
            return <li key={i}><CartItem item={item} key={item.itemId}/></li>;
        });
    },

    _calcTotal() {
        let total = 0;
        this.props.cart.items.forEach(item => {
            total += _.get(item, 'product.price', 0) * _.get(item, 'quantity', 0);
        });
        total = Math.round(total * 100) / 100;
        return total;
    },


});

module.exports = Cart;

