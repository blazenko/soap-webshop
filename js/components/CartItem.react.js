/**
 * @jsx React.DOM
 */

const React = require('react');
const _ = require('lodash');
const Link = require('react-router').Link;

const Actions = require('../actions/shop.actions.js');

const CartItem = React.createClass({



	render() {
		return (
			<div className='cart-item'>
				<h6>
					<Link to={`product/${_.get(this.props, 'item.product.product_id')}`}>{_.get(this.props, 'item.product.product_name')}</Link>
				</h6>
                Kn <span className="cart-item-price item-one">{_.get(this.props, 'item.product.price')}</span>,
				<span className='cart-item-quantity'>{_.get(this.props, 'item.quantity')}</span> kom.,
				ukupno <span className="cart-item-price item-total">{this._getItemTotal()}</span>
				<span className="deselect-product" onClick={this._unSelectProduct}>{this._renderBtnText()}</span>
			</div>
		);
	},

    _getItemTotal() {
		let total = 0;
		total += _.get(this.props, 'item.product.price', 0) * _.get(this.props, 'item.quantity', 0);
		total = Math.round(total * 100) / 100;
		return total;
    },

	_renderBtnText() {
        return <span>&times;</span>;
	},

	_unSelectProduct() {
		Actions.unSelectProduct(this.props.item.itemId);
	}
});

module.exports = CartItem;