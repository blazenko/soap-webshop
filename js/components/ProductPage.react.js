/**
 * @jsx React.DOM
 */

const React = require('react');
const _ = require('lodash');

const Actions = require('../actions/shop.actions.js');
const Store = require('../stores/shop.store.js');

const ProductGallery = require('./ProductGallery.react');

const Product = React.createClass({

    propTypes: {
        params: React.PropTypes.object
    },

    getInitialState() {
        console.log('PP getInitialState');
        return {
            product: {
                product_id: null,
                product_name: null,
                price: null,
                description: '',
                skus: [],
                images: []
            }
        };
    },


    componentDidMount() {
        console.log('PP componentDidMount', this.state);
        Store.addChangeListener(this._onProduct);
        this.setState({
            product: Store.getProduct(this.props.params.id)
        });
    },


    componentDidUpdate(prevProps, prevState) {
        console.log('PP componentDidUpdate', this.state);
        if (prevProps.params.id != this.props.params.id) {
            this.setState({
                product: Store.getProduct(this.props.params.id)
            });
        }
    },


    componentWillUnmount() {
        Store.removeChangeListener(this._onProduct);
    },


	render() {
        console.log('PP render', this.state);
        if (!this.isMounted) {
            return null;
        }
		return (
			<div className='col-sm-12 product-page'>
                <h6>Product {this.props.params.id}</h6>
                <h1 className="product-page-name">{this.state.product.product_name}</h1>

                <ProductGallery images={this.state.product.images} />

                <div className="clear"></div>
                <div className="product-price">
                    <span className="price-label">Cijena </span>
                    {this.state.product.price} Kn
                </div>
                <button className="btn btn-primary select-product" onClick={this._selectProduct}>Stavi u košaricu</button>
                <div className="product-desc">{this.state.product.description}</div>
            </div>
		);
	},

	_selectProduct() {
        Actions.selectProduct(this.state.product.product_id);
	},

    _onProduct() {
        this.setState({
            product: Store.getProduct(this.props.params.id)
        });

    }
});

module.exports = Product;

