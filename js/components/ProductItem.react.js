/**
 * @jsx React.DOM
 */

const React = require('react');
const _ = require('lodash');
const Link = require('react-router').Link;

const Actions = require('../actions/shop.actions.js');

const Product = React.createClass({

    propTypes: {
        product: React.PropTypes.object
    },


	render() {
        const src = `/images/products/${this.props.product.images[0].filename}`;
		return (
            <div className='product-item'>
                <Link to={`product/${this.props.product.product_id}`}><img src={src} className="image-product-item"/></Link>
                <div className="product-item-content">
                    <Link to={`product/${this.props.product.product_id}`}><h4 className="product-item-name">{this.props.product.product_name}</h4></Link>
                    {this.props.product.price} Kn
                    <br/><br/>
                    <span className="select-product  btn btn-primary" onClick={this._selectProduct}>{this._renderBtnText()}</span>
                </div>
            </div>
		);
	},

	_renderBtnText() {
        return 'Stavi u košaricu';
	},

	_selectProduct() {
    	Actions.selectProduct(this.props.product.product_id);
	}
});

module.exports = Product;

