/**
 * @jsx React.DOM
 */

const React = require('react');
const _ = require('lodash');

//import { Router, Route, Link } from 'react-router';
const Actions = require('../actions/shop.actions');
const Store = require('../stores/shop.store.js');

const ProductItem = require('./ProductItem.react.js');


const Products = React.createClass({

    getInitialState() {
        return {
            category: {}
        };
    },


    componentDidMount() {
        Store.addChangeListener(this._onProducts);
        if (this.props.params) {
            if (this.props.params.categoryId !== this.state.category.category_id) Actions.getProducts(this.props.params.categoryId);
        }
        else {
            Actions.getProducts(this.props.categoryId);
        }
    },

    componentDidUpdate() {
        if (this.props.params) {
            if (this.props.params.categoryId !== this.state.category.category_id) Actions.getProducts(this.props.params.categoryId);
        }
    },

    componentWillUnmount() {
        Store.removeChangeListener(this._onProducts);
    },



    render() {

        return (
            <div className="clearfix">
                { /* <h5>Products Page</h5> */ }
                <h4>{this.state.category.category_name}</h4>
                <p>{this.state.category.category_desc}</p>
                {this._renderItems()}
            </div>
        );
    },

    _renderItems() {
        return _.map(this.state.category.products, p => {
            return <ProductItem key={p.product_id} product={p} />;
        });
    },

    _onProducts() {
        this.setState({
            category: Store.getCategory()
        });

    }

});

module.exports = Products;

