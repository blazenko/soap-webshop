/**
 * @jsx React.DOM
 */
const React = require('react');
const _ = require('lodash');
import { Link } from 'react-router';

const Actions = require('../actions/shop.actions.js');
const Store = require('../stores/shop.store.js');
const CartStore = require('../stores/cart.store.js');

const CartSidebar = require('./CartSidebar.react');

const Shop = React.createClass({

    getInitialState() {
        return {
            appStateClean: true,
            categories: [],
            cart: {
                items: []
            }
        }
    },

    componentDidMount() {
        console.log('SHOP MOUNT');
        CartStore.addChangeListener(this._onCartChange);
        Store.addChangeListener(this._onStoreChange);
        Actions.getProducts();
        Actions.getCategoryList();
        this._checkState();
    },


    componentDidUpdate(prevProps, prevState) {
        console.log('SHOP UPDATE');
        if (!this.state.appStateClean) Actions.saveCartToStorage(this.state.cart);
    },

    componentWillUnmount() {
        Store.removeChangeListener(this._onStoreChange);
        CartStore.removeChangeListener(this._onCartChange);
    },


    render() {
        return (
            <div className="container shop-container">
                <div className="row">
                    <div className="col-md-3 shop-sidebar">
                        <br/>
                        <ul className="shop-nav">
                            <li><Link to={`/`}>Shop Home</Link></li>
                            {this._renderCategories()}
                        </ul>
                        <hr/>
                        <ul>
                            {this._renderCartSidebar()}
                        </ul>
                    </div>

                    <div className="col-md-9 products-main-col">
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    },

    _renderCategories() {
        return _.map(this.state.categories, (c, i) => {
            return <li key={i}><Link to={`/products/${c.category_id}`} key={c.category_id}>{c.category_name}</Link></li>
        });
    },

    _renderCartSidebar() {
        if (this.props.location.pathname.indexOf('cart') !== -1) {
            return null;
        }
        return <CartSidebar cart={this.state.cart}/>;
    },


    _onStoreChange() {
        this.setState({
            categories: Store.getCategoryList()
        });

    },

    _onCartChange() {
        this.setState({
            appStateClean: false,
            cart: CartStore.getCart()
        });
    },

    _checkState() {
        if (this.state.appStateClean) {
            Actions.readCartFromStorage();
        }
    }

});

module.exports = Shop;